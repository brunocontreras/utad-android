package com.brunocontreras.model;

import java.util.List;

/**
 * Created by Bruno on 04/03/2015.
 */
public interface IPersistance {

    public List<Photo> getPhotos();
    public Photo addPhoto(Photo photo);
    public void removePhoto(Photo photo);
    public void updatePhoto(Photo photo);

    public List<Category> getCategories();
    public Category addCategory(Category category);
    public void removeCategory(Category category);
}
