package com.brunocontreras.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Bruno on 02/03/2015.
 */
public class CategoryCollection {

    private static CategoryCollection sInstance;
    private IPersistance mPersistance;
    private List<Category> mCategories;

    public static CategoryCollection getInstance() {
        if (sInstance == null) {
            sInstance = new CategoryCollection();
        }
        return sInstance;
    }

    private CategoryCollection() { }

    public void init(IPersistance persistance) {
        mPersistance = persistance;
        update();
    }

    public List<String> getCategoriesNames() {
        List<String> names = new LinkedList<String>();
        for (Category cat : mCategories) {
            String value = String.format("%s (%d)", cat.getName(), cat.getCount());
            names.add(value);
        }
        return names;
    }

    public void update() {
        mCategories = mPersistance.getCategories();
    }

    public Category switchCategory(Category oldCategory, Category newCategory) {
        if (newCategory != null) {
            boolean exists = false;
            for (Category category : mCategories) {
                if (category.getName().equals(newCategory.getName())) {
                    newCategory = category;
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                newCategory = mPersistance.addCategory(newCategory);
            }
        }
        if (oldCategory != null && oldCategory.getCount() <= 1) {
            mPersistance.removeCategory(oldCategory);
        }
        update();
        return newCategory;
    }

    public int getCount() {
        return mCategories.size();
    }

    public Category getCategory(int position) {
        return mCategories.get(position);
    }
}
