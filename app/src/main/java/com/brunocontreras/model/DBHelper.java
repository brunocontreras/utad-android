package com.brunocontreras.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Bruno on 22/01/2015.
 */
public class DBHelper extends SQLiteOpenHelper implements IPersistance {

    private static DBHelper sDBHelper;
    private static String cDateFormat = "yyyy-MM-dd HH:mm:ss";

    // Base de datos
    private static final String DB_NAME = "photogallery";
    private static final int DB_VERSION = 1;

    // Table Photos
    private static final String TABLE_PHOTOS_NAME = "photos";
    private static final String COL_PHOTOS_ID = "_id";
    private static final String COL_PHOTOS_NAME = "name";
    private static final String COL_PHOTOS_DATE = "date";
    private static final String COL_PHOTOS_PATH = "path";
    private static final String COL_PHOTOS_CATEGORYID = "categoryId";
    private static final String COL_PHOTOS_ISFAVORITE = "isfavorite";
    private static final String COL_PHOTOS_LATITUDE = "latitude";
    private static final String COL_PHOTOS_LONGITUDE = "longitude";

    // Table Categories
    private static  final String TABLE_CATEGORIES_NAME = "categories";
    private static final String COL_CATEGORIES_ID = "_id";
    private static final String COL_CATEGORIES_NAME = "name";


    public static synchronized DBHelper getInstance(Context context) {
        if (sDBHelper == null) {
            sDBHelper = new DBHelper(context);
        }
        return sDBHelper;
    }

    private DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.beginTransaction();

        // Crea la tabla de fotos
        String createPhotosTable = String.format(
            "CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, %s TEXT, %s DATETIME, %s TEXT NOT NULL, %s INTEGER, %s BOOL NOT NULL DEFAULT FALSE, %s DOUBLE, %s DOUBLE);",
            TABLE_PHOTOS_NAME,
            COL_PHOTOS_ID,
            COL_PHOTOS_NAME,
            COL_PHOTOS_DATE,
            COL_PHOTOS_PATH,
            COL_PHOTOS_CATEGORYID,
            COL_PHOTOS_ISFAVORITE,
            COL_PHOTOS_LATITUDE,
            COL_PHOTOS_LONGITUDE);
        db.execSQL(createPhotosTable);

        // Crea la tabla de categorías
        String createCategoriesTable = String.format(
                "CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, %s TEXT NOT NULL);",
                TABLE_CATEGORIES_NAME,
                COL_CATEGORIES_ID,
                COL_CATEGORIES_NAME);
        db.execSQL(createCategoriesTable);

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    // Implementación de la interfaz de persistencia
    @Override
    public List<Photo> getPhotos() {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = String.format("SELECT p.%s, p.%s, p.%s, p.%s, p.%s, c.%s, p.%s, p.%s, p.%s FROM %s p LEFT OUTER JOIN %s c ON p.%s = c.%s ORDER BY p.%s DESC;",
                COL_PHOTOS_ID, COL_PHOTOS_NAME, COL_PHOTOS_DATE, COL_PHOTOS_PATH, COL_PHOTOS_CATEGORYID, COL_CATEGORIES_NAME, COL_PHOTOS_ISFAVORITE, COL_PHOTOS_LATITUDE, COL_PHOTOS_LONGITUDE,
                TABLE_PHOTOS_NAME, TABLE_CATEGORIES_NAME, COL_PHOTOS_CATEGORYID, COL_CATEGORIES_ID, COL_PHOTOS_DATE);

        List<Photo> photos = new ArrayList<Photo>();
        Cursor c = db.rawQuery(query, null);
        while (c.moveToNext()) {
            try {
                int id = c.getInt(0);
                String name = c.getString(1);
                SimpleDateFormat dateFormat = new SimpleDateFormat(cDateFormat);
                Date date = dateFormat.parse(c.getString(2));
                String path = c.getString(3);
                Category category = null;
                if (c.getInt(4) != 0) {
                    int categoryId = c.getInt(4);
                    String categoryName = c.getString(5);
                    category = new Category(categoryId, categoryName);
                }
                Boolean isFavorite = c.getInt(6) > 0;
                Double latitude = c.getDouble(7);
                Double longitude = c.getDouble(8);
                Photo photo = new Photo(id, name, date, path, category, isFavorite, latitude, longitude);
                photos.add(photo);
            } catch (ParseException ex) { }
        }
        c.close();
        db.close();
        return photos;
    }

    @Override
    public Photo addPhoto(Photo photo) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues insertValues = new ContentValues(1);
        insertValues.put(COL_PHOTOS_NAME, photo.getName());
        SimpleDateFormat dateFormat = new SimpleDateFormat(cDateFormat);
        insertValues.put(COL_PHOTOS_DATE, dateFormat.format(photo.getDate()));
        insertValues.put(COL_PHOTOS_PATH, photo.getPath());
        insertValues.put(COL_PHOTOS_LATITUDE, photo.getLatitude());
        insertValues.put(COL_PHOTOS_LONGITUDE, photo.getLongitude());
        long id = db.insert(TABLE_PHOTOS_NAME, null, insertValues);
        photo.setId(id);
        db.close();
        return photo;
    }

    @Override
    public void removePhoto(Photo photo) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PHOTOS_NAME, COL_PHOTOS_ID + " = " + photo.getId(), null);
        db.close();
    }

    @Override
    public void updatePhoto(Photo photo) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("UPDATE %s SET %s = '%s'", TABLE_PHOTOS_NAME, COL_PHOTOS_NAME, photo.getName()));
        if (photo.getCategory() != null) {
            stringBuilder.append(String.format(", %s = %d", COL_PHOTOS_CATEGORYID, photo.getCategory().getId()));
        } else {
            stringBuilder.append(String.format(", %s = NULL", COL_PHOTOS_CATEGORYID));
        }
        stringBuilder.append(String.format(", %s = %s", COL_PHOTOS_ISFAVORITE, photo.getIsFavorite() ? 1 : 0));
        if (photo.getLatitude() != null) {
            stringBuilder.append(String.format(", %s = %f", COL_PHOTOS_LATITUDE, photo.getLatitude()));
        } else {
            stringBuilder.append(String.format(", %s = NULL", COL_PHOTOS_LATITUDE));
        }
        if (photo.getLongitude() != null) {
            stringBuilder.append(String.format(", %s = %f", COL_PHOTOS_LONGITUDE, photo.getLongitude()));
        } else {
            stringBuilder.append(String.format(", %s = NULL", COL_PHOTOS_LONGITUDE));
        }
        stringBuilder.append(String.format(" WHERE %s = %d", COL_PHOTOS_ID, photo.getId()));

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(stringBuilder.toString());
        db.close();
    }

    @Override
    public List<Category> getCategories() {
        List<Category> categories = new ArrayList<Category>();
        SQLiteDatabase db = this.getReadableDatabase();

        String query = String.format("SELECT c.%s, c.%s, count(*) FROM %s c LEFT OUTER JOIN %s p ON c.%s = p.%s GROUP BY c.%s;",
                COL_CATEGORIES_ID, COL_CATEGORIES_NAME, TABLE_CATEGORIES_NAME, TABLE_PHOTOS_NAME, COL_CATEGORIES_ID, COL_PHOTOS_CATEGORYID, COL_CATEGORIES_ID);
        Cursor c = db.rawQuery(query, null);
        while (c.moveToNext()) {
            try {
                int id = c.getInt(0);
                String name = c.getString(1);
                int count = c.getInt(2);
                Category category = new Category(id, name, count);
                categories.add(category);
            } catch (Exception ex) { }
        }
        c.close();
        db.close();
        return categories;
    }

    @Override
    public Category addCategory(Category category) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues(1);
        values.put(DBHelper.COL_CATEGORIES_NAME, category.getName());
        long id = db.insert(DBHelper.TABLE_CATEGORIES_NAME, null, values);
        category.setId(id);
        db.close();
        return category;
    }

    @Override
    public void removeCategory(Category category) {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL(String.format("DELETE FROM %s WHERE %s = %d;", TABLE_CATEGORIES_NAME, COL_PHOTOS_ID, category.getId()));
        db.close();
    }
}
