package com.brunocontreras.model;

import android.database.sqlite.SQLiteDatabase;

import java.util.Date;

/**
 * Created by Bruno on 22/01/2015.
 */
public class Photo {

    private long mId;
    private String mName;
    private Date mDate;
    private String mPath;
    private Category mCategory;
    private Boolean mIsFavorite;
    private Double mLatitude;
    private Double mLongitude;

    // Constructores
    public Photo(long id, String name, Date date, String path, Category category, Boolean isFavorite, Double latitude, Double longitude) {
        mId = id;
        mName = name;
        mDate = date;
        mPath = path;
        mCategory = category;
        mIsFavorite = isFavorite;
        mLatitude = latitude;
        mLongitude = longitude;
    }

    public Photo(String name, Date date, String path, Double latitude, Double longitude) {
        this(0, name, date, path, null, false, latitude, longitude);
    }

    // Getters and setters
    public long getId() { return mId; }
    public void setId(long id) { mId = id; }

    public String getName() { return mName; }
    public void setName(String name) {
        this.mName = name;
        PhotoGallery.getInstance().updatePhoto(this);
    }

    public Date getDate() { return mDate; }

    public String getPath() { return mPath; }

    public Category getCategory() { return mCategory; }
    public void setCategory(Category category) {
        category = CategoryCollection.getInstance().switchCategory(this.getCategory(), category);
        this.mCategory = category;
        PhotoGallery.getInstance().updatePhoto(this);
        CategoryCollection.getInstance().update();
    }
    public void removeCategory() {
        this.setCategory(null);
    }

    public Boolean getIsFavorite() { return mIsFavorite; }
    public void setIsFavorite(Boolean isFavorite) {
        mIsFavorite = isFavorite;
        PhotoGallery.getInstance().updatePhoto(this);
    }

    public Double getLatitude() { return mLatitude; }

    public Double getLongitude() { return mLongitude; }
}