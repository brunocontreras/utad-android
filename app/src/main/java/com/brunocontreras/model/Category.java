package com.brunocontreras.model;

/**
 * Created by Bruno on 02/03/2015.
 */
public class Category {
    private long mId;
    private String mName;
    private int mCount;

    public Category(long id, String name, int count) {
        mId = id;
        mName = name;
        mCount = count;
    }

    public Category(long id, String name) {
        this(id, name, 0);
    }

    public Category(String name) { this(0, name, 0); }

    public long getId() { return mId; }
    public void setId(long id) { mId = id; }

    public String getName() { return mName; }
    public void setName(String mName) { this.mName = mName; }

    public int getCount() { return mCount; }
}
