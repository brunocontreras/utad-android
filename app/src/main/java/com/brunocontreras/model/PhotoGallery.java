package com.brunocontreras.model;

import android.content.Context;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Bruno on 22/01/2015.
 */
public class PhotoGallery {

    private static PhotoGallery sInstance = null;
    private List<Photo> mPhotos = new LinkedList<Photo>();
    private Context mContext;
    private IPersistance mPersistance;

    public static PhotoGallery getInstance() {
        if (sInstance == null) {
            sInstance = new PhotoGallery();
        }
        return sInstance;
    }

    private PhotoGallery() { }

    public void init(Context context) {
        mContext = context;

        mPersistance = DBHelper.getInstance(mContext);
        CategoryCollection.getInstance().init(mPersistance);

        mPhotos = mPersistance.getPhotos();
    }

    public void addPhoto(Photo photo) {
        photo = mPersistance.addPhoto(photo);
        mPhotos.add(0, photo);
    }

    public void removePhotoAt(int position) {
        Photo photo = getPhoto(position);

        mPersistance.removePhoto(photo);
        photo.setCategory(null);

        File file = new File(photo.getPath().substring(7));
        file.delete();

        mPhotos.remove(position);
    }

    public void updatePhoto(Photo photo) {
        mPersistance.updatePhoto(photo);
    }

    public void setFilter(int filter) {
        switch (filter) {
            case -2:
                mPhotos = mPersistance.getPhotos();
                break;
            case -1:
                List<Photo> favorites = new LinkedList<Photo>();
                for (Photo photo : mPersistance.getPhotos()) {
                    if (photo.getIsFavorite()) {
                        favorites.add(photo);
                    }
                }
                mPhotos = favorites;
                break;
            default:
                Category category = CategoryCollection.getInstance().getCategory(filter);
                List<Photo> filtered = new LinkedList<Photo>();
                for (Photo photo : mPersistance.getPhotos()) {
                    if (photo.getCategory() != null && photo.getCategory().getId() == category.getId()) {
                        filtered.add(photo);
                    }
                }
                mPhotos = filtered;
                break;
        }
    }

    public int getCount() {
        return mPhotos.size();
    }

    public Photo getPhoto(int position) {
        return mPhotos.get(position);
    }

    public String getUrlPhoto(int position) {
        return mPhotos.get(position).getPath();
    }
}
