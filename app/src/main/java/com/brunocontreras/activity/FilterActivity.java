package com.brunocontreras.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.brunocontreras.model.CategoryCollection;
import com.brunocontreras.photogallery.R;

import java.util.LinkedList;
import java.util.List;

public class FilterActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_filter);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new FilterFragment())
                    .commit();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class FilterFragment extends DialogFragment implements android.content.DialogInterface.OnClickListener {

        public static final int REQUEST_SELECT_FILTER = 0;
        public static final String FILTER_SELECTED = "FILTER_SELECTED";
        private Dialog mDialog;

        public FilterFragment() { }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            super.onCreateDialog(savedInstanceState);
            View root = getActivity().getLayoutInflater().inflate(R.layout.fr_filter, null);

            List<String> names = CategoryCollection.getInstance().getCategoriesNames();
            names.add(0, "Favoritos");
            names.add(0, "Todos");

            ListView listView = (ListView)root.findViewById(R.id.filter_list);
            listView.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, names));
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent();
                    intent.putExtra(FILTER_SELECTED, position - 2);
                    Fragment targetFragment = getTargetFragment();
                    if (targetFragment == null) {
                        Activity activity = getActivity();
                        activity.setResult(Activity.RESULT_OK, intent);
                        activity.finish();
                    }
                    else {
                        targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                        mDialog.dismiss();
                    }
                }
            });

            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
            dialog.setTitle(R.string.action_filter);
            dialog.setView(root);
            dialog.setNegativeButton(android.R.string.no, this);

            mDialog = dialog.create();
            return mDialog;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_NEGATIVE:
                    Fragment targetFragment = getTargetFragment();
                    if (targetFragment == null) {
                        Activity activity = getActivity();
                        activity.setResult(Activity.RESULT_CANCELED);
                        activity.finish();
                    }
                    else {
                        targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, null);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
