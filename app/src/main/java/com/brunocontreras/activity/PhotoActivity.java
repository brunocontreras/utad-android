package com.brunocontreras.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.brunocontreras.model.Category;
import com.brunocontreras.model.CategoryCollection;
import com.brunocontreras.model.Photo;
import com.brunocontreras.model.PhotoGallery;
import com.brunocontreras.photogallery.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

public class PhotoActivity extends ActionBarActivity {

    public static final String EXTRA_IMAGE_INDEX = "image_index";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_photo);
        if (savedInstanceState == null) {

            PlaceholderFragment fr = new PlaceholderFragment();
            fr.setArguments(getIntent().getExtras());

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fr)
                    .commit();
        }

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean defaultValue = super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return defaultValue;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private PhotoGallery mPhotoGallery;
        private View mRootView;
        private int mPosition;
        private Photo mPhoto;
        private Menu mMenu;
        private DisplayImageOptions options;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);

            mPhotoGallery = PhotoGallery.getInstance();

            options = new DisplayImageOptions.Builder()
                    .resetViewBeforeLoading(true)
                    .cacheOnDisk(true)
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .considerExifParams(true)
                    .displayer(new FadeInBitmapDisplayer(300))
                    .build();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            mRootView = inflater.inflate(R.layout.fr_photo, container, false);

            // Obtiene la foto que se va a mostrar y actualiza la cabecera
            mPosition = getArguments().getInt(PhotoActivity.EXTRA_IMAGE_INDEX, 0);
            mPhoto = PhotoGallery.getInstance().getPhoto(mPosition);
            updateActionBar();

            ViewPager pager = (ViewPager) mRootView.findViewById(R.id.pager);
            pager.setAdapter(new ImageAdapter());
            pager.setCurrentItem(mPosition);
            pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

                @Override
                public void onPageSelected(int position) {
                    mPosition = position;
                    mPhoto = PhotoGallery.getInstance().getPhoto(mPosition);
                    updateLookAndFeel();
                }

                @Override
                public void onPageScrollStateChanged(int state) { }
            });

            return mRootView;
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            super.onCreateOptionsMenu(menu, inflater);
            inflater.inflate(R.menu.menu_photo, menu);
            mMenu = menu;
            updateMenus();
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            boolean defaultValue = super.onOptionsItemSelected(item);

            switch (item.getItemId()) {

                case R.id.action_favorite:
                    mPhoto.setIsFavorite(!mPhoto.getIsFavorite());
                    updateLookAndFeel();
                    return true;

                case R.id.action_delete:

                    AlertDialog.Builder deleteAlert = new AlertDialog.Builder(getActivity());
                    deleteAlert.setTitle(R.string.action_delete);
                    deleteAlert.setMessage(R.string.action_delete_warning);
                    deleteAlert.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mPhotoGallery.removePhotoAt(mPosition);
                            startActivity(new Intent(getActivity(), GalleryActivity.class));
                        }
                    });
                    deleteAlert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    deleteAlert.show();
                    return true;

                case R.id.action_change_name:
                    AlertDialog.Builder nameAlert = new AlertDialog.Builder(getActivity());
                    nameAlert.setTitle(R.string.action_change_name);

                    final EditText nameInput = new EditText(getActivity());
                    nameInput.setText(mPhoto.getName());
                    nameAlert.setView(nameInput);
                    nameAlert.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String newName = nameInput.getText().toString();
                            mPhoto.setName(newName);
                            updateLookAndFeel();
                        }
                    });
                    nameAlert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    nameAlert.show();
                    return true;

                case R.id.action_change_category:
                    AlertDialog.Builder categoryAlert = new AlertDialog.Builder(getActivity());
                    categoryAlert.setTitle(R.string.action_change_category);

                    final EditText catInput = new EditText(getActivity());
                    if (mPhoto.getCategory() != null) {
                        catInput.setText(mPhoto.getCategory().getName());
                    }
                    categoryAlert.setView(catInput);
                    categoryAlert.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Category category = new Category(catInput.getText().toString());
                            mPhoto.setCategory(category);
                            updateLookAndFeel();
                        }
                    });
                    categoryAlert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    categoryAlert.show();
                    return true;

                case R.id.action_delete_category:
                    if (mPhoto.getCategory() != null) {
                        mPhoto.removeCategory();
                        updateLookAndFeel();
                    }
                    return true;

                default:
                    return defaultValue;
            }
        }

        private void updateActionBar() {
            getActivity().setTitle(mPhoto.getName());

            ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
            actionBar.setTitle(mPhoto.getName());

            if (mPhoto.getCategory() != null) {
                actionBar.setSubtitle(mPhoto.getCategory().getName());
            } else {
                actionBar.setSubtitle("");
            }
        }

        private void updateMenus() {
            MenuItem favoriteItem = mMenu.findItem(R.id.action_favorite);
            favoriteItem.setIcon(mPhoto.getIsFavorite() ? R.drawable.ic_favorite_yes : R.drawable.ic_favorite_no);

            MenuItem deleteCategoryItem = mMenu.findItem(R.id.action_delete_category);
            deleteCategoryItem.setVisible(mPhoto.getCategory() != null);
        }

        private void updateLookAndFeel() {
            updateActionBar();
            updateMenus();
        }

        private class ImageAdapter extends PagerAdapter {

            private LayoutInflater inflater;

            ImageAdapter() {
                inflater = LayoutInflater.from(getActivity());
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((View) object);
            }

            @Override
            public int getCount() {
                return mPhotoGallery.getCount();
            }

            @Override
            public Object instantiateItem(ViewGroup view, int position) {
                View imageLayout = inflater.inflate(R.layout.item_pager_image, view, false);
                assert imageLayout != null;
                ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);
                final ProgressBar spinner = (ProgressBar) imageLayout.findViewById(R.id.loading);

                ImageLoader.getInstance().displayImage(mPhotoGallery.getUrlPhoto(position), imageView, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        spinner.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        String message = null;
                        switch (failReason.getType()) {
                            case IO_ERROR:
                                message = "Input/Output error";
                                break;
                            case DECODING_ERROR:
                                message = "Image can't be decoded";
                                break;
                            case NETWORK_DENIED:
                                message = "Downloads are denied";
                                break;
                            case OUT_OF_MEMORY:
                                message = "Out Of Memory error";
                                break;
                            case UNKNOWN:
                                message = "Unknown error";
                                break;
                        }
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        spinner.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        spinner.setVisibility(View.GONE);
                    }
                });

                view.addView(imageLayout, 0);
                return imageLayout;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view.equals(object);
            }

            @Override
            public void restoreState(Parcelable state, ClassLoader loader) { }

            @Override
            public Parcelable saveState() {
                return null;
            }
        }
    }
}
