package com.brunocontreras.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.brunocontreras.model.Photo;
import com.brunocontreras.model.PhotoGallery;
import com.brunocontreras.photogallery.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GalleryActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_gallery);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    // Fragmento
    public static class PlaceholderFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

        private static final int REQUEST_IMAGE_CAPTURE = 1;

        private PhotoGallery mPhotoGallery;
        private View mRootView;
        private GridView mGridView;

        private String mImagePath;
        private String mImageName;

        private GoogleApiClient mGoogleApiClient;
        private DisplayImageOptions options;
        private ImageLoader mImageLoader;
        private ImageAdapter mAdapter;

        private Location mLastLocation;
        private double mLatitude;
        private double mLongitude;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);

            mPhotoGallery = PhotoGallery.getInstance();

            options = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.color.background_floating_material_dark)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .build();

            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            mRootView = inflater.inflate(R.layout.fr_gallery, container, false);

            mGridView = (GridView) mRootView.findViewById(R.id.grid);

            mAdapter = new ImageAdapter();
            mGridView.setAdapter(mAdapter);
            mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getActivity(), PhotoActivity.class);
                    intent.putExtra(PhotoActivity.EXTRA_IMAGE_INDEX, position);
                    startActivity(intent);
                }
            });

            return mRootView;
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            super.onCreateOptionsMenu(menu, inflater);
            inflater.inflate(R.menu.menu_gallery, menu);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            boolean defaultValue = super.onOptionsItemSelected(item);
            switch (item.getItemId()) {
                case R.id.action_filter:
                    FilterActivity.FilterFragment dialog = new FilterActivity.FilterFragment();
                    dialog.setTargetFragment(this, FilterActivity.FilterFragment.REQUEST_SELECT_FILTER);
                    dialog.show(getFragmentManager(), null);
                    return true;
                case R.id.action_camera:
                    takePicture();
                    return true;
                default:
                    return defaultValue;
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                Photo photo = new Photo(mImageName, new Date(), mImagePath, mLatitude, mLongitude);
                mPhotoGallery.addPhoto(photo);
                mAdapter.notifyDataSetChanged();
            } else {
                if (requestCode == FilterActivity.FilterFragment.REQUEST_SELECT_FILTER && resultCode == RESULT_OK) {
                    int selected = data.getIntExtra(FilterActivity.FilterFragment.FILTER_SELECTED, -2);
                    mPhotoGallery.setFilter(selected);
                    mAdapter.notifyDataSetChanged();
                }
            }
        }

        // ***** Funciones privadas

        // Lanza la cámara para obtener una foto
        private void takePicture() {
            mGoogleApiClient.connect();
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                File photoFile = null;
                try {
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    mImageName = "JPEG_" + timeStamp;
                    File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                    photoFile = File.createTempFile(mImageName, ".jpg", storageDir);
                    mImagePath = "file://" + photoFile.getAbsolutePath();
                } catch (IOException ex) { }

                if (photoFile != null) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        }

        @Override
        public void onConnected(Bundle bundle) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                mLatitude = mLastLocation.getLatitude();
                mLongitude = mLastLocation.getLongitude();
            }
        }

        @Override
        public void onConnectionSuspended(int i) {

        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {

        }

        public class ImageAdapter extends BaseAdapter {

            private LayoutInflater mInflater;

            ImageAdapter() {
                mInflater = LayoutInflater.from(getActivity());
            }

            @Override
            public int getCount() {
                return mPhotoGallery.getCount();
            }

            @Override
            public Object getItem(int position) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                final ViewHolder holder;
                View view = convertView;
                if (view == null) {
                    view = mInflater.inflate(R.layout.item_grid_image, parent, false);
                    holder = new ViewHolder();
                    assert view != null;
                    holder.imageView = (ImageView) view.findViewById(R.id.image);
                    holder.progressBar = (ProgressBar) view.findViewById(R.id.progress);
                    view.setTag(holder);
                } else {
                    holder = (ViewHolder) view.getTag();
                }

                ImageLoader.getInstance().displayImage(mPhotoGallery.getUrlPhoto(position), holder.imageView, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        holder.progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        holder.progressBar.setVisibility(View.GONE);
                    }
                }, null);

                return view;
            }
        }

        static class ViewHolder {
            ImageView imageView;
            ProgressBar progressBar;
        }
    }
}
