package com.brunocontreras.application;

import android.app.Application;

import com.brunocontreras.model.PhotoGallery;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

/**
 * Created by Bruno on 22/02/2015.
 */
public class PGApplication extends Application {

    @Override
    public void onCreate() {

        // Crea la configuración global de la librería para mostrar las imágenes
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024) // 50 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app
                .build();

        // Inicializa la librería con la configuración personalizada
        ImageLoader.getInstance().init(config);

        // Inicializa la galería de fotos, para pasar el contexto para la base de datos.
        PhotoGallery.getInstance().init(getApplicationContext());
    }
}
